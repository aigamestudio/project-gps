﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BattleInputBehaviour : MonoBehaviour
{
    public BattleStatus battleStatus { get; private set; } = BattleStatus.NotReady;

    public int currentLevel { get; private set; }
    public float currentActiveCharm { get; private set; }
    public float currentPassiveCharm { get; private set; }

    public Target currentTarget { get; private set; }
    public float currentTargetHealth { get; private set; }

    public float currentTime { get; private set; }

    private void Start()
    {
        if (Application.isEditor)
            Application.runInBackground = true;

        // setup darah status dkk
        currentLevel = PlayerPrefs.GetInt("battleInput_currentLevel", 0);
        currentActiveCharm = PlayerPrefs.GetFloat("gameInput_currentActiveCharm", 1.0f);
        currentPassiveCharm = PlayerPrefs.GetFloat("gameInput_currentPassiveCharm", 0.0f);

        currentTarget = Database.Instance.Targets[currentLevel];
        currentTargetHealth = currentTarget.health / 2;
        currentTime = currentTarget.timeLimit;
    }

    private void Update()
    {
        HandleCheckWin();
    }

    private void HandleCheckWin()
    {
        battleStatus = (currentTargetHealth >= currentTarget.health || currentTime <= 0) ? BattleStatus.Lose : 
                        (currentTargetHealth <= 0) ? BattleStatus.Win : battleStatus;
    }

    private IEnumerator TargetActiveExorciseCoroutine()
    {
        UIBattleBehaviour ui = GetComponent<UIBattleBehaviour>();
        while(battleStatus == BattleStatus.NotOver)
        {
            yield return new WaitForSeconds(1.0f / currentTarget.exorcisePerSecond);
            currentTargetHealth = Mathf.Min(currentTargetHealth + currentTarget.activeExorcise, currentTarget.health);
            ui.ExorciseEffect();
        }
    }

    private IEnumerator EverySecondCoroutine()
    {
        UIBattleBehaviour ui = GetComponent<UIBattleBehaviour>();
        while (battleStatus == BattleStatus.NotOver)
        {
            yield return new WaitForSeconds(1.0f);
            currentTargetHealth = Mathf.Clamp(currentTargetHealth + currentTarget.passiveExorcise - currentPassiveCharm, 0, currentTarget.health);
            currentTime = Mathf.Max(0, currentTime - 1);
            ui.PassiveBattleEffect();
        }
    }

    public void PrepareLove()
    {
        battleStatus = BattleStatus.NotOver;
        StartCoroutine(TargetActiveExorciseCoroutine());
        StartCoroutine(EverySecondCoroutine());
    }

    public void SendLove()
    {
        currentTargetHealth = Mathf.Max(currentTargetHealth - currentActiveCharm, 0);
    }

    public void ExitGame()
    {
        if (battleStatus == BattleStatus.Win) currentLevel++;
        PlayerPrefs.SetInt("battleInput_currentLevel", currentLevel);
    }

    public void Surrender()
    {
        currentTime = 0;
    }
}

public enum BattleStatus
{
    NotReady,
    NotOver,
    Win,
    Lose
}
