﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimateText : MonoBehaviour
{
    public static IEnumerator ShowWithAnimateText(Text text, string strComplete, float speed)
    {
        int i = 0;
        string str = "";
        while (i < strComplete.Length)
        {
            str += strComplete[i++];
            text.text = str;
            yield return new WaitForSeconds(speed * Time.deltaTime);
        }
    }
}
