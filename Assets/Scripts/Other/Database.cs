﻿using UnityEngine;
using System.Collections;

/**
 * Kelas Singleton biar gampang diakses darimana aja
 * Isinya data-data yang gampang diganti sama game desainer
 **/
public class Database : MonoBehaviour
{
    public static Database Instance { get; private set; }

    public Item[] Items;
    public Target[] Targets;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        } else
        {
            Destroy(gameObject);
        }

    }
}
