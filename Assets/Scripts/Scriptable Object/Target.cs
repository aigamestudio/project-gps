﻿using UnityEngine;
using System.Collections;

/**
 * Scriptable Object dari kelas Drink
 * Buat nambah Drink baru tinggal bikin scriptable object baru
 **/
[CreateAssetMenu(fileName = "New Target", menuName = "Target")]
public class Target : ScriptableObject
{
    public new string name;
    public RuntimeAnimatorController animatorController;

    public float activeExorcise;
    public float exorcisePerSecond;
    public float passiveExorcise;

    public float timeLimit;
    public float health;

    public string passiveVoiceLine;
    public string[] activeVoiceLine;

    [TextArea(1,3)] public string winDescLine;
}
