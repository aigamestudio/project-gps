﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Scriptable Object dari kelas Drink
 * Buat nambah Drink baru tinggal bikin scriptable object baru
 **/
[CreateAssetMenu(fileName = "New Item", menuName = "Item")]
public class Item : ScriptableObject
{
    public int id;
    public new string name;
    public Sprite itemSprite;

    public float initialCost;
    public float initialActiveCharm;
    public float initialPassiveCharm;
    public float upgradeRatioCost;
    public float upgradeRatioActiveCharm;
    public float upgradeRatioPassiveCharm;
    public int maxUpgrade = 10;
    public int minLevelToBuy = 0;
}
