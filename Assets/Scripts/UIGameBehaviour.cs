﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Globalization;

/**
 * Kelas yang ngatur view dari Game Input
 * Isinya ngeset text, atur galon button, sama inisiasi awal dari Drink
 **/
public class UIGameBehaviour : MonoBehaviour
{
    GameInputBehaviour gameInput;

    [SerializeField] Text _duitText;
    [SerializeField] Text _kharismaText;
    [SerializeField] Text _kecantikanText;

    [SerializeField] Animator _kuntiAnimator;

    [SerializeField] RectTransform _itemPanelParent;
    [SerializeField] UIItemPanelBehaviour _itemPanelPrefab;
    UIItemPanelBehaviour[] itemPanels;

    [SerializeField] RectTransform _textEffect;

    [SerializeField] AfterPDKTNarative[] _afterPDKTNaratives;
    [SerializeField] AfterPDKTNarative[] _afterPDKTNarativesFreeVersion;

    int currentItem;

    private void Awake()
    {
        gameInput = GetComponent<GameInputBehaviour>();
    }

    private void Start()
    {
        currentItem = PlayerPrefs.GetInt("uiGameInput_currentItem", 0);

        // inisiasi seberapa banyak drink yang bisa dibeli
        itemPanels = new UIItemPanelBehaviour[Database.Instance.Items.Length];
        for(int i=0; i<itemPanels.Length; i++)
        {
            itemPanels[i] = Instantiate(_itemPanelPrefab, _itemPanelParent, false);
            itemPanels[i]._gameInput = gameInput;
            itemPanels[i]._itemScriptable = Database.Instance.Items[i];
        }

        // ganti jadi sampe current item
        for(int i=0; i<currentItem+1; i++)
        {
            itemPanels[i].gameObject.SetActive(true);
        }

        if(PlayerPrefs.GetString("uiGameInput_currentLevel-" + gameInput.currentLevel + "_storyIsDone", false.ToString()) == false.ToString())
        {
            AfterPDKTNarative afterPDKTNarative = (!gameInput.isFreeVersion) ? 
                _afterPDKTNaratives[gameInput.currentLevel] : _afterPDKTNarativesFreeVersion[gameInput.currentLevel];
            UINotificationPanelBehaviour.Instance.OpenNotification(afterPDKTNarative.title, afterPDKTNarative.desc);
            PlayerPrefs.SetString("uiGameInput_currentLevel-" + gameInput.currentLevel + "_storyIsDone", true.ToString());
            return;
        }

        if(PlayerPrefs.GetString("uiGameInput_sleepNotifIsDone", false.ToString()) == false.ToString())
        {
            UINotificationPanelBehaviour.Instance.OpenNotification("Jangan di Close Gamenya", "Kunti bobok kalo lu keluar / pause dari gamenya." +
                "\nTerus dia ga kerja selama lu keluar / pause gamenya." +
                "\nJadi jangan diapa-apain yak wkwkwk");
            PlayerPrefs.SetString("uiGameInput_sleepNotifIsDone", true.ToString());
        }
    }

    private void Update()
    {
        _duitText.text = gameInput.currentMoney.ToString("C2", CultureInfo.GetCultureInfo("id-ID"));
        _kharismaText.text = gameInput.currentActiveCharm.ToString("0.00");
        _kecantikanText.text = gameInput.currentPassiveCharm.ToString("0.00");
        _kuntiAnimator.SetFloat("currentPassiveCharm", gameInput.currentPassiveCharm);
        
        // cek udah beli apa belum drink sebelumnya, kalo udah munculin yang selanjutnya
        if(currentItem + 1 < itemPanels.Length && itemPanels[currentItem].currentAmount > 0)
        {
            currentItem++;
            itemPanels[currentItem].gameObject.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
    }

    private void OnApplicationPause(bool pause)
    {
        if (!pause)
        {
            UINotificationPanelBehaviour.Instance.OpenNotification("Jangan di Close Gamenya", "Kuntiii bobok kalo lu keluar / pause dari gamenya." +
                "\nTerus dia ga kerja selama lu keluar / pause gamenya." +
                "\nJadi jangan diapa-apain yak wkwkwk");
            PlayerPrefs.SetString("uiGameInput_sleepNotifIsDone", true.ToString());
        }
    }

    private void OnApplicationQuit()
    {
        gameInput.SaveGame();
        SaveItems();

        PlayerPrefs.SetString("uiGameInput_sleepNotifIsDone", false.ToString());
    }

    private void SaveItems()
    {
        PlayerPrefs.SetInt("uiGameInput_currentItem", currentItem);
        for (int i = 0; i < itemPanels.Length; i++)
        {
            itemPanels[i].SaveItem();
        }
    }

    public void ActiveCharmButton()
    {
        gameInput.AddMoneyByActiveCharm();

        // bikin efek +1
        RectTransform textEffect = Instantiate(_textEffect, transform, false);
        textEffect.position = _kuntiAnimator.transform.GetChild(0).GetChild(0).position;
        textEffect.GetComponentInChildren<Text>().text = "+" + (gameInput.currentActiveCharm * gameInput.charmToMoneyRatio).ToString("C2", CultureInfo.GetCultureInfo("id-ID"));
        Destroy(textEffect.gameObject, 1.0f);
    }

    public void PDKTButton(Animator transition)
    {
        Target currentTarget = Database.Instance.Targets[gameInput.currentLevel];
        if (gameInput.currentActiveCharm >= currentTarget.activeExorcise && gameInput.currentPassiveCharm >= currentTarget.passiveExorcise)
        {
            transition.SetTrigger("close");
            gameInput.SaveGame();
            SaveItems();
            StartCoroutine(ChangeScene("PDKTScene"));
        }
        else
        {
            UINotificationPanelBehaviour.Instance.OpenNotification("Kuntiii Masih Belum Siap Nih Buat PDKT",
                "Shopping produk dan jasa kecantikan dulu sebelum PDKT biar Kuntiii siap nembak doi \n" +
                "Yang kurang :\n\n" +
                "Kharisma  minimal " + currentTarget.activeExorcise + "\n" +
                "Kecantikan minimal " + currentTarget.passiveExorcise
                );
        }
    }

    IEnumerator ChangeScene(string sceneName)
    {
        yield return new WaitForSeconds(1.0f);
        SceneManager.LoadScene(sceneName);
    }
}

[System.Serializable]
public class AfterPDKTNarative
{
    public string title;
    [TextArea(1,10)] public string desc;
}