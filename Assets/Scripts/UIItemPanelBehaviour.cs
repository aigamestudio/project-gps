﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;

/**
 * View dari Drink nya
 * Ngatur deskripsi text, sama ngatur fungsi ketika buy
 **/
public class UIItemPanelBehaviour : MonoBehaviour
{
    public GameInputBehaviour _gameInput { private get; set; }
    public Item _itemScriptable { private get; set; }

    [SerializeField] Button _buyButton;
    [SerializeField] Image _itemImage;
    [SerializeField] Text _titleText;
    [SerializeField] Text _costText;
    [SerializeField] Text _activeCharmText;
    [SerializeField] Text _passiveCharmText;

    public float currentActiveCharm { get; private set; } = 0.0f;
    public float currentPassiveCharm { get; private set; } = 0.0f;
    public int currentAmount { get; private set; } = 0;
    float currentCost = 0;

    private void Start()
    {
        LoadItem();
        RefreshBuyButton();
        RefreshDescText();
    }

    private void Update()
    {
        // tambah cek kalo levelnya udah cukup apa belom 
        _buyButton.interactable = currentAmount < _itemScriptable.maxUpgrade && 
                                _gameInput.currentMoney >= currentCost && 
                                _gameInput.currentLevel >= _itemScriptable.minLevelToBuy;
    }

    private void LoadItem()
    {
        currentAmount = PlayerPrefs.GetInt("uiItemPanel_id-" + _itemScriptable.id + "_currentAmount", 0);

        //generate cost, active passive
        currentCost = GenerateCost(currentAmount);
        currentActiveCharm = GenerateActiveCharm(currentAmount);
        currentPassiveCharm = GeneratePassiveCharm(currentAmount);
    }

    private float GenerateCost(int amount)
    {
        if (amount == 0) return _itemScriptable.initialCost;
        return GenerateCost(amount - 1) + amount * _itemScriptable.upgradeRatioCost;
    }
    
    private float GenerateActiveCharm(int amount)
    {
        if (amount == 0) return 0;
        if (amount == 1) return _itemScriptable.initialActiveCharm;
        return GenerateActiveCharm(amount - 1) + amount * _itemScriptable.upgradeRatioActiveCharm;
    }

    private float GeneratePassiveCharm(int amount)
    {
        if (amount == 0) return 0;
        if (amount == 1) return _itemScriptable.initialPassiveCharm;
        return GeneratePassiveCharm(amount - 1) + amount * _itemScriptable.upgradeRatioPassiveCharm;
    }

    private void RefreshBuyButton()
    {
        if(_gameInput.currentLevel < _itemScriptable.minLevelToBuy)
        {
            _buyButton.GetComponentInChildren<Text>().text = "Kurang PDKT";
            return;
        }

        if (currentAmount >= _itemScriptable.maxUpgrade)
        {
            _buyButton.GetComponentInChildren<Text>().text = "Max!";
            return;
        }

        if (currentAmount > 0)
        {
            _buyButton.GetComponentInChildren<Text>().text = "Upgrade";
        }
    }

    private void RefreshDescText()
    {
        _costText.text = (currentAmount >= _itemScriptable.maxUpgrade) ? "Udah brand termahal" : "- " +currentCost.ToString("C2", CultureInfo.GetCultureInfo("id-ID"));
        _activeCharmText.text = "+" + currentActiveCharm.ToString("0.00");
        _passiveCharmText.text = "+" + currentPassiveCharm.ToString("0.00");

        if(currentAmount > 0)
        {
            _itemImage.sprite = _itemScriptable.itemSprite;
            _titleText.text = _itemScriptable.name + " (Lv. " + currentAmount + ")";
        }
    }

    public void BuyButton()
    {
        _gameInput.DecreaseMoneyByBuying(currentCost);
        _gameInput.UpdateActiveCharm((currentAmount == 0) ? _itemScriptable.initialActiveCharm : (currentAmount + 1) * _itemScriptable.upgradeRatioActiveCharm);
        _gameInput.UpdatePassiveCharm((currentAmount == 0) ? _itemScriptable.initialPassiveCharm : (currentAmount + 1) * _itemScriptable.upgradeRatioPassiveCharm);
        currentAmount++;

        currentCost = GenerateCost(currentAmount); // sini ngaco
        currentActiveCharm = GenerateActiveCharm(currentAmount); // lu juga
        currentPassiveCharm = GeneratePassiveCharm(currentAmount); // apalagi lu

        RefreshBuyButton();
        RefreshDescText();
        SaveItem();
    }

    public void SaveItem()
    {
        PlayerPrefs.SetInt("uiItemPanel_id-" + _itemScriptable.id + "_currentAmount", currentAmount);
    }
}
