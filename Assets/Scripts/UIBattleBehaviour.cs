﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIBattleBehaviour : MonoBehaviour
{
    BattleInputBehaviour _battleInput;

    [SerializeField] RectTransform _gameplayPanel;
    [SerializeField] RectTransform _notReadyPanel;
    [SerializeField] RectTransform _notOverPanel;

    [SerializeField] RectTransform _gameOverPanel;
    [SerializeField] RectTransform _winPanel;
    [SerializeField] RectTransform _losePanel;

    [SerializeField] RectTransform _health;
    [SerializeField] RectTransform _loveHealth;
    [SerializeField] RectTransform _spawnerKuntiHealth;
    [SerializeField] RectTransform _spawnerTargetHealth;
    [SerializeField] UIBattleEffectBehaviour _battleActiveKuntiPrefab;
    [SerializeField] UIBattleEffectBehaviour _battlePassiveKuntiPrefab;
    [SerializeField] UIBattleEffectBehaviour _battleActiveTargetPrefab;
    [SerializeField] UIBattleEffectBehaviour _battlePassiveTargetPrefab;

    [SerializeField] Text _timeText;
    [SerializeField] Text _targetNameText;

    [SerializeField] Animator _kuntiAnimator;
    [SerializeField] Animator _targetAnimator;

    [SerializeField] MusicBehaviour _music;
    [SerializeField] Text _winDescText;

    private void Awake()
    {
        _battleInput = GetComponent<BattleInputBehaviour>();
    }

    private void Start()
    {
        _targetNameText.text = _battleInput.currentTarget.name;
        _targetAnimator.runtimeAnimatorController = _battleInput.currentTarget.animatorController;

        StartCoroutine(GameOverCoroutine());
    }

    private void Update()
    {
        _gameplayPanel.gameObject.SetActive(_battleInput.battleStatus == BattleStatus.NotReady || _battleInput.battleStatus == BattleStatus.NotOver);
        _notReadyPanel.gameObject.SetActive(_battleInput.battleStatus == BattleStatus.NotReady);
        _notOverPanel.gameObject.SetActive(_battleInput.battleStatus == BattleStatus.NotOver);

        _kuntiAnimator.SetInteger("battleStatus", (int)_battleInput.battleStatus);
        _targetAnimator.SetInteger("battleStatus", (int)_battleInput.battleStatus);

        if (_battleInput.battleStatus == BattleStatus.NotReady || _battleInput.battleStatus == BattleStatus.NotOver)
        {
            float newPosition = (_battleInput.currentTarget.health - _battleInput.currentTargetHealth) * _health.sizeDelta.x / _battleInput.currentTarget.health;
            _health.anchoredPosition = new Vector2(newPosition, _health.anchoredPosition.y);
            _loveHealth.anchoredPosition = new Vector2(_health.anchoredPosition.x - _health.sizeDelta.x / 2, _health.anchoredPosition.y);

            _timeText.text = "" + (int)_battleInput.currentTime;
        }
    }

    private IEnumerator GameOverCoroutine()
    {
        while (!(_battleInput.battleStatus == BattleStatus.Lose || _battleInput.battleStatus == BattleStatus.Win))
            yield return null;

        _music.StopMusic();

        yield return (_battleInput.battleStatus == BattleStatus.Lose) ? new WaitForSeconds(1.0f) : new WaitForSeconds (3.0f);
        _gameOverPanel.gameObject.SetActive(_battleInput.battleStatus == BattleStatus.Win || _battleInput.battleStatus == BattleStatus.Lose);
        _winPanel.gameObject.SetActive(_battleInput.battleStatus == BattleStatus.Win);
        _losePanel.gameObject.SetActive(_battleInput.battleStatus == BattleStatus.Lose);

        if (_battleInput.battleStatus == BattleStatus.Lose)
        {
            yield return new WaitForSeconds(5.0f);
            _battleInput.ExitGame();
            if (_battleInput.currentLevel > 0) SceneManager.LoadScene("GameplayScene");
            else SceneManager.LoadScene("PDKTScene");
        } else if (_battleInput.battleStatus == BattleStatus.Win)
        {
            StartCoroutine(AnimateText.ShowWithAnimateText(_winDescText, _battleInput.currentTarget.winDescLine, 0.25f));
        }
    }

    private void InstantiateEffect(UIBattleEffectBehaviour prefab, Transform parent, bool isActiveEffect)
    {
        UIBattleEffectBehaviour effect = Instantiate(prefab, parent, false);
        effect.isActiveEffect = isActiveEffect;
        effect._loveHealth = _loveHealth;
        effect.currentTarget = _battleInput.currentTarget;
    }

    public void LoveButton()
    {
        if(_battleInput.battleStatus == BattleStatus.NotReady)
        {
            _battleInput.PrepareLove();
            _music.PlayMusic();
            return;
        }

        if(_battleInput.battleStatus == BattleStatus.NotOver)
        {
            _battleInput.SendLove();
            InstantiateEffect(_battleActiveKuntiPrefab, _spawnerKuntiHealth, true);
        }
    }

    public void PassiveBattleEffect()
    {
        if(_battleInput.currentPassiveCharm > 0.0f) InstantiateEffect(_battlePassiveKuntiPrefab, _spawnerKuntiHealth, false);
        if (_battleInput.currentTarget.passiveExorcise > 0.0f) InstantiateEffect(_battlePassiveTargetPrefab, _spawnerTargetHealth, false);
    }

    public void ExorciseEffect()
    {
        InstantiateEffect(_battleActiveTargetPrefab, _spawnerTargetHealth, true);
    }

    public void PauseButton(bool isResume)
    {
        Time.timeScale = (isResume) ? 1 : 0;
        _music.PauseMusic(!isResume);
    }

    public void SurrenderButton()
    {
        _battleInput.Surrender();
    }

    public void WinButton() { StartCoroutine(ChangeSceneWin()); }
    IEnumerator ChangeSceneWin()
    {
        yield return new WaitForSeconds(1.0f);
        _battleInput.ExitGame();
        if(_battleInput.currentLevel < Database.Instance.Targets.Length)
            SceneManager.LoadScene("GameplayScene");
        else
            SceneManager.LoadScene("CreditScene");
    }
}
