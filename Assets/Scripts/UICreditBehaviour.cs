﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UICreditBehaviour : MonoBehaviour
{
    [SerializeField] RectTransform _creditPanel;

    private void OnEnable()
    {
        int currentLevel = PlayerPrefs.GetInt("battleInput_currentLevel", 0);
        if (currentLevel == 0)
        {
            SceneManager.LoadScene("PDKTScene");
        }
        else if (currentLevel < Database.Instance.Targets.Length)
        {
            SceneManager.LoadScene("GameplayScene");
        }
        else
        {
            _creditPanel.gameObject.SetActive(true);
        }
    }

    public void NewGameButton()
    {
        PlayerPrefs.DeleteAll();
        SceneManager.LoadScene("PDKTScene");
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
