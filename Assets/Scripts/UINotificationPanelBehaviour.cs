﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UINotificationPanelBehaviour : MonoBehaviour
{
    public static UINotificationPanelBehaviour Instance;

    [SerializeField] RectTransform _transitionPanel;

    [SerializeField] Text _titleText;
    [SerializeField] Text _descText;

    string currentTitle;
    string currentDesc;

    private void Awake()
    {
        Instance = this;
        gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        _transitionPanel.gameObject.SetActive(true);
        _titleText.text = currentTitle;
        StartCoroutine(AnimateText.ShowWithAnimateText(_descText, currentDesc, 0.25f));
    }

    private void OnDisable()
    {
        _transitionPanel.gameObject.SetActive(false);
    }

    public void OpenNotification(string title, string desc)
    {
        SetTitle(title);
        SetDesc(desc);
        gameObject.SetActive(true);
    }

    public void SetTitle(string title) { currentTitle = title; }
    public void SetDesc(string desc) { currentDesc = desc; }
}
