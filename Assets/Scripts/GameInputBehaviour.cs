﻿using UnityEngine;
using System.Collections;

/**
 * Core dari gameplay
 * Isinya data dari uang saat ini dan gps saat ini
 **/
public class GameInputBehaviour : MonoBehaviour
{
    public float currentMoney { get; private set; }
    public float currentActiveCharm { get; private set; }
    public float currentPassiveCharm { get; private set; }
    public int currentLevel { get; private set; }

    public float charmToMoneyRatio { get; private set; } = 1000;
    public bool isFreeVersion = false;
    [SerializeField] float charmToMoneyRatioFreeVersion = 100;

    private void Start()
    {
        if (Application.isEditor)
            Application.runInBackground = true;

        LoadGame();

        if (isFreeVersion) charmToMoneyRatio = (currentLevel > 1) ? charmToMoneyRatioFreeVersion : charmToMoneyRatio;

        StartCoroutine(PassiveCharmCoroutine());
    }
    
    private void LoadGame()
    {
        currentMoney = PlayerPrefs.GetFloat("gameInput_currentMoney", 0.0f);
        currentActiveCharm = PlayerPrefs.GetFloat("gameInput_currentActiveCharm", 1.0f);
        currentPassiveCharm = PlayerPrefs.GetFloat("gameInput_currentPassiveCharm", 0.0f);
        currentLevel = PlayerPrefs.GetInt("battleInput_currentLevel", 0);
    }

    private IEnumerator PassiveCharmCoroutine()
    {
        // naikin uang sesuai gps
        while(true)
        {
            currentMoney += currentPassiveCharm * charmToMoneyRatio;
            yield return new WaitForSeconds(1.0f);
        }
    }
    
    public void AddMoneyByActiveCharm()
    {
        currentMoney += currentActiveCharm * charmToMoneyRatio;
    }

    public void DecreaseMoneyByBuying(float money)
    {
        currentMoney -= money;
    }

    public void UpdateActiveCharm(float upgradeActiveCharm)
    {
        currentActiveCharm += upgradeActiveCharm;
    }

    public void UpdatePassiveCharm(float upgradePassiveCharm)
    {
        currentPassiveCharm += upgradePassiveCharm;
    }

    public void SaveGame()
    {
        PlayerPrefs.SetFloat("gameInput_currentMoney", currentMoney);
        PlayerPrefs.SetFloat("gameInput_currentActiveCharm", currentActiveCharm);
        PlayerPrefs.SetFloat("gameInput_currentPassiveCharm", currentPassiveCharm);
    }
}
