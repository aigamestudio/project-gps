﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIBattleEffectBehaviour : MonoBehaviour
{
    RectTransform rectTransform;
    Text text;

    public RectTransform _loveHealth { private get; set; }
    public bool isActiveEffect { private get; set; }
    public Target currentTarget { private get; set; }

    float parentXPos;
    
    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        text = GetComponent<Text>();
    }

    private void Start()
    {
        if (isActiveEffect)
        {
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, Random.Range(-4.0f, 4.0f));
            if (text) text.text = currentTarget.activeVoiceLine[Random.Range(0, currentTarget.activeVoiceLine.Length)];
        } else
        {
            if (text) text.text = currentTarget.passiveVoiceLine;
        }

        parentXPos = transform.parent.GetComponent<RectTransform>().anchoredPosition.x;
    }

    private void Update()
    {
        float targetPosX = Mathf.MoveTowards(rectTransform.anchoredPosition.x, _loveHealth.anchoredPosition.x - parentXPos, 100.0f * Time.deltaTime);
        rectTransform.anchoredPosition = new Vector2(targetPosX, rectTransform.anchoredPosition.y);
        if (Mathf.Abs(rectTransform.anchoredPosition.x - (_loveHealth.anchoredPosition.x - parentXPos)) < 1f) Destroy(gameObject);
    }

}
