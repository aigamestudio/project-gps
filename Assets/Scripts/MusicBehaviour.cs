﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicBehaviour : MonoBehaviour
{
    AudioSource _audioSource;

    // ganti audio mixer

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void StopMusic() { _audioSource.Stop(); }
    public void PauseMusic(bool isPause) { if (isPause) _audioSource.Pause(); else _audioSource.UnPause(); }
    public void PlayMusic() { _audioSource.Play(); }
}
